const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const penShema = new Schema({

        code : String ,
        
        name : String ,

        description : String ,

        categories :
            {
             type : Schema.Types.ObjectId ,
             ref : 'categories' 
             } ,

        color : [ 
             {
                 type : String 
                
            
            } ] ,

        ink : [{
            type : String 
        }] ,

        priceMM : {
            type : Number
        } ,

        priceUS : {
            type : Number
        } ,
        
        photos : [
            {type : String}
        ]


})


mongoose.model('pens' , penShema)










