
const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;


const userSchema = new Schema({

        google : {
            id : String ,
            username : String ,
            email : String ,
            admin : {
              type : Boolean ,
              default : false 
            } ,
          

        } ,
       
        facebook : {
          id : String ,
          username : String ,
          email : String ,
          admin : {
            type : Boolean ,
            default : false 
          },
       
        } ,

        local : {
          username : {
            type : String ,
           
            index : true
          } ,
          email : {
            type : String ,
          
          } ,
          password : {
            type : String ,
        
          } ,
          admin : {
            type : Boolean ,
            default : false 
          } 
       
        } ,

         provider : {
           type : String ,
          
         }
       
        

})


// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};




mongoose.model('users' , userSchema);