const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const cartSchema = new Schema({


    

        owner : {
            type : Schema.Types.ObjectId ,
            ref : 'users'
        },

        items : [{
                item : {type : Schema.Types.ObjectId , ref : 'products'},
                quantity : {type : Number ,  default : 0} ,
                price : {type : Number , default : 0 } ,
                color : {type : String },
                inkcolor : {type : String }
        }]


})

mongoose.model('carts' , cartSchema)