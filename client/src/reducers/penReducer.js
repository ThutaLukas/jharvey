import { GET_PENS , GET_PEN } from "../actions/types";

const initialState = {
        pens : [] ,
        pen : {}
}

export default (state = initialState, action) => {
  switch (action.type) {

  case GET_PENS:
    return {
         ...state,
            pens : action.payload
                
        }
        case GET_PEN:
        return {
             ...state,
                pen : action.payload
                    
            }

  default:
    return state
  }
}
