import {FETCH_USER , SET_CURRENT_USER} from '../actions/types';
import isEmpty from '../utils/isEmpty'

const initialState = {
    isAuthenticated : false,
    user : {}
}


export default ( state = initialState   , action) => {
      
        switch (action.type) {
            
            case FETCH_USER : 
            return {
                ...state ,
                isAuthenticated : !isEmpty(action.payload) ,
                user : action.payload 
            } ;
            break;
           
            default:
            return state ;
              
        }
    
}