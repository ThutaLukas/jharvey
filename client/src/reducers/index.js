import { combineReducers} from 'redux';
import authReducer from './authReducer';
import categoryReducer from './categoryReducer'
import penReducer from './penReducer'
import {reducer as reduxForm} from 'redux-form';


export default combineReducers({
    auth : authReducer ,
    category : categoryReducer ,
    pen : penReducer,
    form : reduxForm
})