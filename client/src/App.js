import React, { Component } from 'react';
import {BrowserRouter as Router , Route , Switch , Redirect } from 'react-router-dom';
import './App.css';
import PrivateRoute from './utils/PrivateRoute'


import {getCookieValue} from './utils/jwtUtils'


// REDUX
import { connect } from 'react-redux';
import { fetchUser} from './actions/auth/authActions'

// google analytics
import Analytics from 'react-router-ga';


import HomePage from './components/Landing/HomePage/Homepage';
import DashBoard from './components/DashBoard/DashBoard';
import DashBoardHeader from './components/DashBoard/DashBoardHeader/DashBoardHeader'
import CreatePen from './components/DashBoard/Pen/CreatePen/CreatePen';
import CreateCategory from './components/DashBoard/Pen/CreateCategory/CreateCategory'
import PenPage from './components/DashBoard/Pen/PenPage/PenPage'
import ClientOrders from './components/DashBoard/ClientOrders/ClientOrders'
import ContactUs from './components/Landing/HomePage/ContactUs/ContactUs'
import Header from './components/Landing/Navigation/Header/Header'
import SinglePen from './components/DashBoard/Pen/PenPage/SinglePen/SinglePen'
import Cart from './components/DashBoard/Cart/Cart'

class App extends Component {
 
 
 
  
  
  componentDidMount = () => {
   
    
     const token = getCookieValue('jwt');
         if(token){
          this.props.fetchUser()
         }
    
  }
  
  
  
  
  
  
  render() {
    

   
          return (
            <div className="">
                     <Router>
                        <Analytics  id="UA-122113546-1" >
                    <div>
                        
                      <Switch>   
                              
                                <Route exact path="/" component={HomePage}  />  
                                <Route path="/pens" component={PenPage}/>
                                <Route path="/contact" component={ContactUs}/>
                                 <Route path="/pen/:id" component={SinglePen}  />      
                                 <Route path="/carts" component={Cart}  />      
                                                                                      
                                  <div>
                                      <DashBoardHeader/>
                                       <Switch>
                                     
                                          <PrivateRoute  exact path="/dashboard" component={DashBoard}  /> 
                                          <PrivateRoute    path="/dashboard/create-pen" component={CreatePen}  /> 
                                          <PrivateRoute    path="/dashboard/create-category" component={CreateCategory}  />                                         
                                          <PrivateRoute    path="/dashboard/orders" component={ClientOrders}  /> 
                                       </Switch>
                                   </div> 
                                        
                                      
                             
                       </Switch>
                        
                     
                                                                                
                              
                                 
                                                   
                       
                    </div>
                

                  </Analytics>
                </Router>       
            </div>
          );
        }
}





const mapStateToProps = (state) => ({
    auth : state.auth
})

const mapDispatchToProps = {
  fetchUser 
}

export default connect(mapStateToProps , mapDispatchToProps)(App);
