// REACT CORE MODULES
import React from 'react';
import ReactDOM from 'react-dom';


// STYLING MODULES
import 'materialize-css/dist/css/materialize.min.css';


// REDUX MODULES
import {createStore , applyMiddleware, combineReducers , compose} from 'redux';
import { connect  , Provider} from 'react-redux';
import thunk from 'redux-thunk';
import reducer from './reducers/index';



//COMPONENT MODULES
import App from './App';


// GLOBAL STORE
const store = createStore(  
    // all reducers
     reducer , 
   
    // inital store state >> empty object
    {}  ,
   
    // middlewares here!
   compose(
    applyMiddleware(thunk) ,
   // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() 
   ) 
    
    
    
    );




 // MAIN REACT APP ROOT and REDUX MAIN STORE CONFIG
ReactDOM.render(

     <Provider store={store}>

       
            <App />
       
         

    </Provider> 

        ,
document.getElementById('root')


);



