export const FETCH_USER = 'fetch_user';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const GET_CATEGORIES = 'GET_CATEGORIES'
export const GET_PENS = 'GET_PENS'
export const GET_PEN = 'GET_PEN'