import axios from 'axios';
import {FETCH_USER} from '../types';
import setAuthToken from '../../utils/setAuthToken'
import jwt_decode from 'jwt-decode';
import {getCookieValue} from '../../utils/jwtUtils'
 
export const fetchUser   =   () => {
    

 

       return (dispatch) => {
        const token = getCookieValue('jwt')  ;
       
         const decoded = jwt_decode(token);
         dispatch({type : FETCH_USER  , payload : decoded });
       }
       
       
       
  
    
                  
  
}






export const logoutUser = () => dispatch => {
  
 
    

  var delete_cookie = function(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

    delete_cookie('jwt')
  // Set current user to {} which will set isAuthenticated to false
     dispatch({type : FETCH_USER , payload : {}});
  window.location.href = '/';
  
};


