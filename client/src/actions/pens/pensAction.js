
import {GET_CATEGORIES , GET_PENS , GET_PEN} from '../types';
import axios from 'axios'




export const addCategory = (category) => {
    
    
    
    return   (dispatch) => {

             axios.post('/api/category/add' , category)
                .then((res) => {
                        console.log(category)
                })
            

    }
}


export const addPen = (pen) => {
    
    
    
        return   (dispatch) => {
    
                 axios.post('/api/pen/create' , pen)
                    .then((res) => {
                            console.log(res.data)
                    })
                
    
        }
    }


export const getCategories = () => {


        return  (dispatch) => {

             axios.get('/api/category/get')
                .then((res) => {
                    dispatch({
                            type : GET_CATEGORIES ,
                            payload : res.data
                    })
                })
                .catch((err) => {
                  alert(err)
                })

                




        }


}


export const getPens = () => {


        return  (dispatch) => {

             axios.get('/api/pen/get')
                .then((res) => {
                    dispatch({
                            type : GET_PENS ,
                            payload : res.data
                    })
                })
                .catch((err) => {
                  alert(err)
                })

                




        }


}

export const getPen = (id) => {


        return  (dispatch) => {

             axios.get(`/api/pen/get/${id}`)
                .then((res) => {
                    dispatch({
                            type : GET_PEN ,
                            payload : res.data
                    })
                })
                .catch((err) => {
                  alert(err)
                })

                




        }


}