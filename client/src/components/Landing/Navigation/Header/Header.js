

import React, { Component  , Fragment} from 'react'
import M from 'materialize-css/dist/js/materialize.min.js';
import {NavLink } from 'react-router-dom'
import Badge from '@material-ui/core/Badge';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux'


import './Header.css'
 





    class Header extends Component {
  
  
        state = {
            anchorEl: null,
          };

          handleClick = event => {
            this.setState({ anchorEl: event.currentTarget });
          };
        
          handleClose = () => {
            this.setState({ anchorEl: null });
          };

  
            componentDidMount = () => {
                M.AutoInit();
            }
            
  
  
  
  
  
  
        render() {
        
            const {anchorEl} = this.state;
            const {auth} = this.props;

            const NavBarWide = (

                        <Fragment>
                                <li> <NavLink to="/pens">Pens</NavLink></li>
                                <li> <NavLink to="/stores">Stores Available</NavLink></li>
                                <li> <NavLink to="/activities">Activities</NavLink></li>                              
                                { auth.isAuthenticated === true  ?
                         <li> <a href="/dashboard"><button className="btn btn-round pink lighten-3">dashboard</button></a></li>
                         : 
                         <li> <a href="/auth/facebook"><button className="btn btn-round pink lighten-3">login to shop</button></a></li>
                        }   

                                <li> 
                                    <a
                                             id="language-menu"
                                             onClick={this.handleClick}
                                    >
                                        
                                             <i className="fas fa-globe-asia"></i>
                                     </a>                                   
                                  
                                </li>
                              


                                <li>
                                <a href="/carts">
                                  
                                     <i className="fas fa-shopping-cart">
                                     <Badge 
                                         badgeContent={0}
                                         color="secondary"
                                         style={{paddingTop : '25px'}}
                                    >
                                   </Badge>
                                     </i>
                                  
                                   
                                </a>
                                </li>
                               
                        </Fragment>

            )


            const mobileMenu = (
                <ul className="sidenav" id="mobile-demo">


                      <li> <img src="https://res.cloudinary.com/heliosbots/image/upload/v1525446992/heliosbots/ILOVELAMP.png" className="header-logo center"/></li>
                      <li> <NavLink to="/pens">Pens</NavLink></li>
                      <li> <NavLink to="/stores">Stores Available</NavLink></li>
                      <li> <NavLink to="/activities">Activities</NavLink></li>    
                      { auth.isAuthenticated === true  ?
                         <li> <a href="/dashboard"><button className="btn btn-round pink lighten-3">dashboard</button></a></li>
                         : 
                         <li> <a href="/auth/facebook"><button className="btn btn-round pink lighten-3">login to shop</button></a></li>
                        }                          
                     
                                   
                </ul>
            )

         const LanguageMenu = (
            <Menu
                id="language-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
               onClose={this.handleClose}
             >
                <MenuItem onClick={this.handleClose}>ျမန္မာ</MenuItem>
                <MenuItem onClick={this.handleClose}>English</MenuItem>

            </Menu>

         )
        
            return (
                 <nav className="transparent">
                   <div className="nav-wrapper ">
                              
                              
                              <div className="container">
                                     <a href="/" className="brand-logo">
                                      J<span className="red-text">&hearts;</span>Harvey                                 
                                    </a>
                                </div>
                                <a href="#" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a>
                               



                        <ul  className="right hide-on-med-and-down">
                              {NavBarWide}
                            
                        </ul>

                        <ul className="right hide-on-large-only  ">
                                

                                  <li> 
                                    <a
                                     id="language-menu"
                                     onClick={this.handleClick}
                                    >
                                    <i className="fas fa-globe-asia"></i>
                                     </a>                                   
                                  
                                </li>


                                <li>
                                <a href="/carts">                                 
                                     <i className="fas fa-shopping-cart">
                                     <Badge 
                                         badgeContent={0}
                                         color="secondary"
                                         style={{paddingTop : '25px'}}
                                    >
                                   </Badge>
                                     </i>                                 
                                </a>
                                </li>
                        </ul>






                             {mobileMenu}

                             {LanguageMenu}


                    

                        
                   </div>     
                 </nav>
        )
    }
    }


const mapStateToProps = (state) => ({
  auth : state.auth
})

// const mapDispatchToProps = {
  
// }





export default connect(mapStateToProps)(Header) ;