

import React, { Component } from 'react'
import { Field, reduxForm , FieldArray } from 'redux-form'
import isEmpty from '../../../../utils/isEmpty'
import MenuItem from '@material-ui/core/MenuItem';

import Select from '@material-ui/core/Select';
import './CreatePenForm.css'
 
   


class CreatePenForm extends Component {
  



  
        render() {


            const { handleSubmit, pristine, reset, submitting , handleSubmitHandler , categories } = this.props;
           
            const renderSelectField = ({ input, label, meta: { touched, error },  }) => (
              
              
              <Select
                style={{width : '70%'}}
                {...input}
                onChange={(value) => input.onChange(value)}
              
              
              >

                    {
                      categories.map((category) => {

                            return (
                              <MenuItem value={category._id} primaryText="Red">
                               {category.name}
                             </MenuItem>

                            )
                      })
                    }
                   
                           
                </Select>
            )
         
            const renderColors = ({fields, meta: {error}}) => (
                <ul>
                  <li>
                    <button type="button" className="btn  pink lighten-3" onClick={() => fields.push()}>add color</button>
                  </li>
                  {fields.map((color , index) => (
                    <li key={index}>
                      <button
                        type="button"
                        title="Remove color"
                        onClick={() => fields.remove(index)}
                        className="btn  pink lighten-3"
                        placeholder="remove color"
                      >
                       remove
                      </button>
                      <Field
                        name={color}
                        type="text"
                        component="input"
                        label={`Paragraph ${index + 1}`}
                      
                      />
                    </li>
                  ))}
                 
                </ul>
              )
        
              const renderInk = ({fields, meta: {error}}) => (
                <ul>
                  <li>
                    <button type="button" className="btn  pink lighten-3" onClick={() => fields.push()}>add ink color</button>
                  </li>
                  {fields.map((ink , index) => (
                    <li key={index}>
                      <button
                        type="button"
                        title="Remove ink"
                        onClick={() => fields.remove(index)}
                        className="btn  pink lighten-3"
                        placeholder="remove ink"
                      >
                       remove
                      </button>
                      <Field
                        name={ink}
                        type="text"
                        component="input"
                        label={`Paragraph ${index + 1}`}
                      
                      />
                    </li>
                  ))}
                 
                </ul>
              )
               

              const renderPhotos = ({fields, meta: {error}}) => (
                <ul>
                  <li>
                    <button type="button" className="btn  pink lighten-3" onClick={() => fields.push()}>add photo</button>
                  </li>
                  {fields.map((photo , index) => (
                    <li key={index}>
                      <button
                        type="button"
                        title="Remove photo"
                        onClick={() => fields.remove(index)}
                        className="btn  pink lighten-3"
                        placeholder="remove photo"
                      >
                       remove
                      </button>
                      <Field
                        name={photo}
                        type="text"
                        component="input"
                        label={`Paragraph ${index + 1}`}
                      
                      />
                    </li>
                  ))}
                 
                </ul>
              )
        
         






         
 
        
            return (
            
            
                <form onSubmit={handleSubmitHandler} className="container">


                     <div className="input-field col s12">
                        <Field
                             name="code"
                             component="input"
                             type="text"
                             placeholder="code"
                             />
                      
         
                   </div>     


                     <div className="input-field col s12">
                        <Field
                             name="name"
                             component="input"
                             type="text"
                             placeholder="name"
                             />
                      
         
                   </div>     

                  <div className="input-field col s12">
                   <Field
                         name="description"
                          type="text"
                        component="textarea"
                      placeholder="add description"
                      className="materialize-textarea"
                    />
                      
         
                   </div>   

                     <div className="input-field col s12">

                     {
                        !isEmpty(categories) ? 
                        <Field
                               name="category"
                               component={renderSelectField}
                        /> 
                         
                       
                          
                     
                        : null 

                     }
                      
         
                   </div>   


                     <div className="input-field col s12">
                        <Field
                             name="priceMM"
                             component="input"
                             type="text"
                             placeholder="Kyat price"
                             />
                      
         
                   </div>   

                      <div className="input-field col s12">
                        <Field
                             name="priceUS"
                             component="input"
                             type="text"
                             placeholder="USD price"
                             />
                      
         
                     </div>   


                       <div className="input-field col s12">
                        <FieldArray
                             name="color"
                             component={renderColors}
                           
                             />
                      
         
                     </div>   

                         <div className="input-field col s12">
                        <FieldArray
                             name="ink"
                             component={renderInk}
                           
                             />
                      
         
                     </div>   

                     <div className="input-field col s12">
                        <FieldArray
                             name="photos"
                             component={renderPhotos}
                           
                             />
                      
         
                     </div>   


                   


                   <div className="input-field col s12 center-align">
                         
                         <button className="btn pink lighten-3 addpen" type="submit" disabled={pristine || submitting}>
                             add pen
                         </button>
               
  
                    </div>  




                </form>
            )
        }
    }




    
    

    export default reduxForm({
        form : "createPen"

    })(CreatePenForm);
