

import React, { Component } from 'react'
import { connect } from 'react-redux'

import {getPen} from '../../../../../actions/pens/pensAction'
import isEmpty from '../../../../../utils/isEmpty'
import Divider from '@material-ui/core/Divider';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import './SinglePen.css'




 class SinglePen extends Component {
   
   
   componentDidMount = () => {
       
        const id = this.props.match.params.id
        this.props.getPen(id);
   }
   
   state = {
       color : '' ,
       ink : ''
   }
   
   
    render() {
        
        const {pen} = this.props.pen;
        const { isAuthenticated} = this.props.auth;
        let singlePenContent;
        let photos;
        let colors;
        let goBackLink;

     
        if(pen === null || undefined) {
                singlePenContent = (
                    <h3>Loading please wait</h3>
                )
        }else {
         

            
            
            singlePenContent = (

                   
                                
                         
            <div className="card-panel">
                 <div className="singlepen-card">
                         <a className="right btn btn-small pink lighten-3" href="/pens">back to pens</a>
                              <h4 className="center singlepen-headertext">{pen.name}</h4>
                                                              
                                 <Divider  />
                                                               
                             <div className="card-panel">
                                         <div className="row">
                                                    <div className="col s12 m6 l6">
                                                        <ul className="addtocart">
                                                            <li><button className="btn pink lighten-3">add to cart</button></li>
                                                            {/* <li><a className="btn-floating btn-small waves-effect waves-light pink lighten-3"><i className="material-icons">exposure_plus_1</i></a></li>
                                                            <li><a className="btn-floating btn-small waves-effect waves-light pink lighten-3"><i className="material-icons">exposure_neg_1</i></a></li> */}
                                                            
                                                            <li> 
                                                                
                                                            <a href="/carts">
                                    
                                                            <i className="fas fa-shopping-cart">
                                                            <Badge 
                                                                badgeContent={0}
                                                                color="secondary"
                                                                style={{paddingTop : '25px'}}
                                                            >
                                                            </Badge>
                                                            </i>
                                
                                    
                                                            </a></li>
                                                            
                                                          
                                                    </ul> 
                                                    </div>
                                                    <div className="col s12 m6 l6" >
                                                    <ul className="collection">
                                                      
                                                        <li className="collection-item"><div>price<a href="#!" className="secondary-content">{pen.priceMM} kyats</a></div></li>
                                                        <li className="collection-item"><div>price in USD<a  className="secondary-content">{pen.priceUS} dollars</a></div></li>
                                                        <li> <Select
                                                           style={{width : '100%'}}
                                                        >
                                                          
                                                           
                                                            <MenuItem value={10}>Ten</MenuItem>
                                                            <MenuItem value={20}>Twenty</MenuItem>
                                                            <MenuItem value={30}>Thirty</MenuItem>
                                                        </Select></li>
                                                        <li> <Select
                                                              style={{width : '100%'}}
                                                           >
                                                             
                                                              
                                                               <MenuItem value={10}>Ten</MenuItem>
                                                               <MenuItem value={20}>Twenty</MenuItem>
                                                               <MenuItem value={30}>Thirty</MenuItem>
                                                           </Select></li>
                                                    </ul>
                                                    </div>
                                                    
                                         </div>                                  
                              </div>
                                <div  className="card-panel">
                                                <div className="center">
                                                   <h4> 0 kyats | 0 dollars</h4>
                                                   <h5> 0  quantity</h5>
                                                </div>
                                          

                                </div>
                              <div className="card-panel">
                                        <h5 className="center">Why {pen.name}</h5>
                                        <p className="center"style={{marginTop : '20px'}}>{pen.description}</p>


                              </div>


                                                                

                  </div>
                                                
              </div>
                               
                       
                            

                           



                  

               )     



        }







        return (
        <div className="singlepen-bk">
              <div className="container">
                     <div className="row">
                     {singlePenContent}
                    </div>  
                 
              </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => ({
  pen : state.pen,
  auth : state.auth
})

const mapDispatchToProps = {
    getPen
}




export default connect(mapStateToProps , mapDispatchToProps)(SinglePen);