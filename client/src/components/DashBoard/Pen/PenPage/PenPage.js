

import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import {getPens} from '../../../../actions/pens/pensAction'
import {Link} from 'react-router-dom'
import Header from '../../../Landing/Navigation/Header/Header'
import Divider from '@material-ui/core/Divider';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';


class PenPage extends Component {
   
   
   
   componentDidMount = () => {
     this.props.getPens()
   }
   
   
   state = {
       open : false
   }
   
   handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
   
   
   
   
     render() {

        const {pens} = this.props.pen;
        const {auth , fullScreen} = this.props;
        let penContent;

        const navBar = (
            <nav className="pink lighten-3">
                    <ul>
                        <li> <a href="/">Back to Home Page</a></li>
                        {this.props.auth.isAuthenticated ?   <li> <a href="/dashboard">DashBoard</a></li> : null }
                    
                    </ul> 
            </nav>
        )

        if(pens === null || undefined ) {

                penContent = (
                    <div>
                        <h3>wait a minute</h3>
                    </div>
                )

        }else {



             penContent = pens.map((pen) => {

                        return (
                         
                              
                            <div className="col s12 m12 l6">
                              <div className="card">
                                <div className="card-image">
                                  <img src={pen.photos[0]}/>
                                
                              
                                 {auth.isAuthenticated ? 
                                 
                                 <Link className="btn-floating halfway-fab waves-effect waves-light pink lighten-3"
                                 to={`/pen/${pen._id}`}
                                >
                                <i className="material-icons"
                               >add
                                
                                </i>

                                </Link>      
                                
                                
                                
                                
                                    : 
                                    <div>
                                        <a className="btn-floating halfway-fab waves-effect waves-light pink lighten-3" onClick={this.handleClickOpen}> <i className="material-icons"
                               >add
                                
                                </i></a>
                                             <Dialog
                                            fullScreen={fullScreen}
                                            open={this.state.open}
                                            onClose={this.handleClose}
                                          
                                            >
                                            <DialogTitle id="responsive-dialog-title">{"please login with facebook to shop"}</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    <a className="btn blue" href="/auth/facebook">Login With Facebook</a>
                                                </DialogContentText>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={this.handleClose} color="primary">
                                                 close
                                                </Button>
                                              
                                            </DialogActions>
                                            </Dialog>
                                    



                                    </div>
                                   
                                }
                                  
                                </div>
                                <div className="card-content">
                                <span className="card-title">{pen.name}</span>
                                <span className="">{pen.priceMM} kyats </span> or  <span className="">{pen.priceUS} dollars </span>
                               
                                </div>
                              </div>
                            </div>
                        
                       


                        )


            })



    


        }










        return (


                <div className="">
                           

                            <div className="row">
                                    {navBar}
                                        <div className="container"> 
                                                <div className="row" style={{marginTop : '20px'}}>
                                                     {penContent}
                                                </div>
                                     
                                        </div>
                                   

                            </div>
                </div>
        )
    }
    }

const mapStateToProps = (state) => ({
   pen : state.pen,
   auth : state.auth
})

const mapDispatchToProps = {
    getPens
}



export default connect(mapStateToProps , mapDispatchToProps)(withMobileDialog()(PenPage));