

import React, { Component } from 'react'
import { Field, reduxForm  } from 'redux-form'
 



class CreateCategoryForm extends Component {
     
    
    
    
   
    
    

    
    
    render() {

        const { handleSubmit, pristine, reset, submitting , handleSubmitHandler } = this.props;
      
            return (
          
                  
                <form onSubmit={handleSubmitHandler} className="container">
                      
                       <div className="input-field col s12">
                          <Field
                             name="catName"
                             component="input"
                             type="text"
                             placeholder="category"
                             />

         
                       </div>  
                     
                       <div className="input-field col s12 center-align">
                         
                                <button className="btn pink lighten-3" type="submit" disabled={pristine || submitting}>
                                    add category
                                </button>
                      
         
                       </div>  
                     






                </form>
            )
        }
}


export default reduxForm({
    form : "createCategory"
})(CreateCategoryForm) ;
