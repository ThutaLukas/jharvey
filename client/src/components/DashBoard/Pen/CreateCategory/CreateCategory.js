

import React, { Component } from 'react'
import CreateCategoryForm from './CreateCategoryForm'
import { connect } from 'react-redux'

import {addCategory , getCategories} from '../../../../actions/pens/pensAction'
import isEmpty from '../../../../utils/isEmpty'
import {isExistCat} from '../../../../utils/check'




class CreateCategory extends Component {
      
    
    
    componentDidMount = () => {
      this.props.getCategories()
    }
    
    
    
    handleSubmitHandler = (e) => {
           
                          
            const category = {
                name : this.props.form.values.catName
            }

            if(isExistCat(this.props.form.values.catName , this.props.category.categories)) {
                this.props.addCategory(category)
                alert(`u added  category ${category.name}`)
            }else {
                alert(`this category is already added. sorry `)
            }   
          
          

    }
    
    
    
    
    
    
    render() {
        const {categories} = this.props.category;
        
       let categoriesList;
       if(isEmpty(categories)) {

                categoriesList = (
                      <ul className="collection with-header center">
                    <li className="collection-header "><h4>Categories</h4></li>
                     </ul>
                )
       }else {
              categoriesList = (
                    

                  <ul className="collection with-header center">
                    <li className="collection-header "><h4> Your Categories</h4></li>
                    {
                            categories.map((category) => {
                                        return (
                                            <li key={category._id} className="collection-item">{category.name}</li>
                                        )
                            })




                    }
                  </ul>



              )
       }
       
        
        
        
        
        return (
            <div className="container">

                        <div className="row">
                          <h3 className="center">Create Category</h3>
                           <div className="col s12 card-panel">
                                <CreateCategoryForm handleSubmitHandler={this.handleSubmitHandler}/>
                           </div>
                          
                        </div>
                        <div className="row">
                                    
                             
                                <div className="col s12 card-panel">
                                    {categoriesList}
                                </div> 
                       </div>
                   
            </div>
            )
        }
    }


const mapStateToProps = (state) => ({
    form : state.form.createCategory ,
    category : state.category
})

const mapDispatchToProps = {
    addCategory ,
    getCategories
    
}


export default connect(mapStateToProps , mapDispatchToProps )(CreateCategory); 

