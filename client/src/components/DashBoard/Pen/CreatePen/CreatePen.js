

import React, { Component } from 'react'
import CreatePenForm from '../CreatePenForm/CreatePenForm'
 import { connect } from 'react-redux'
 import {getCategories , addPen} from '../../../../actions/pens/pensAction'
 import isEmpty from '../../../../utils/isEmpty'



class CreatePen extends Component {
  
  
   componentDidMount = () => {
    this.props.getCategories()
  }
  


    handleSubmitHandler = (e) => {
            
           
        const newPen = {
                code : this.props.form.values.code ,
                name : this.props.form.values.name ,
                description : this.props.form.values.description ,
                categories : this.props.form.values.category ,
                color : this.props.form.values.color ,
                ink : this.props.form.values.ink ,
                priceMM : this.props.form.values.priceMM ,
                priceUS : this.props.form.values.priceUS ,
                photos : this.props.form.values.photos

        } 
       
        this.props.addPen(newPen)

        alert( `you create a new pen called ${newPen.name}` )
     


    }




  
  
        render() {
                const {categories} = this.props.category
                let CPContent;
            if(isEmpty(categories)){
                    CPContent = (
                         <p>please add one category</p>
                    )

            }else {
                CPContent = (
                    <CreatePenForm handleSubmitHandler={this.handleSubmitHandler}  categories={categories}   />
                )
            }
 
            
           
           
            return (
                    <div className="container">                   
                        <div className="row">                                                                           
                                <h3 className="center">Create Pen</h3>
                                   
                                    <div className="col s12 card-panel">
                                  
                                                {CPContent}

                                    </div>
                                 

                               
                              

                                
                                


                           


                        </div>
                    </div>
            )
        }
    }



const mapStateToProps = (state) => ({
  form : state.form.createPen ,
  category : state.category
})

const mapDispatchToProps = {
    getCategories , 
    addPen
}




export default connect(mapStateToProps , mapDispatchToProps)(CreatePen);
