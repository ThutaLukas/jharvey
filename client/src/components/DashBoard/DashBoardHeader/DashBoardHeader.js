
import React, { Component } from 'react'
import { connect } from 'react-redux'
import {logoutUser} from '../../../actions/auth/authActions';
 import './DashBoardHeader.css';
 import M from 'materialize-css/dist/js/materialize.min.js';
 import Badge from '@material-ui/core/Badge';
 import {NavLink} from 'react-router-dom'


class DashBoardHeader extends Component {
  
  
  componentDidMount = () => {
    M.AutoInit();
  }
  
  
  
  

   
   
   
   
    render() {
      
      
        const mobileMenu = (
            <ul className="sidenav" id="mobile-demo">


                  <li> <img src="https://res.cloudinary.com/heliosbots/image/upload/v1525446992/heliosbots/ILOVELAMP.png" className="header-logo center"/></li>
                  <li ><a href="/orders">Client Orders</a></li>
                  <li ><a href="/dashboard/create-pen">Create Pen</a></li>
                  <li ><a href="/dashboard/create-category">Create Category</a></li>
                   <li ><a href="/">Back to Home Page</a></li>                       
                   <li ><button className="btn pink lighten-4" onClick={ () => {this.props.logoutUser()}}>Log Out </button></li>
                               
            </ul>
        )
      




        return (
          
            <nav className="pink lighten-3">
              <div className="nav-wrapper container">
                       
                     <a href="/dashboard" className="brand-logo">Your Orders</a>
                     <a href="/dashboard" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a>
                     
              
                        <ul id="nav-mobile" className="right hide-on-med-and-down dashboard-head">                           
                           
                        
                            <li ><NavLink to="/dashboard/orders">Client Orders</NavLink></li>
                            <li ><NavLink to="/dashboard/create-pen">Create Pen</NavLink></li>
                            <li ><NavLink to="/dashboard/create-category">Create Category</NavLink></li>
                            <li ><a href="/">Back to Home Page</a></li>
                            <li >
                                <a href="/carts">
                                  
                                     <i className="fas fa-shopping-cart">
                                     <Badge 
                                         badgeContent={0}
                                         color="secondary"
                                         style={{paddingTop : '25px'}}
                                    >
                                   </Badge>
                                     </i>
                                  
                                   
                                </a>
                                </li>
                            <li ><button className="btn pink lighten-4" onClick={ () => {this.props.logoutUser()}}>Log Out </button></li>
                           
                        </ul>
                        {mobileMenu}  

                <ul className="right hide-on-large-only  ">
                                

                             


                              <li>
                              <a>                                 
                                   <i className="fas fa-shopping-cart">
                                   <Badge 
                                       badgeContent={0}
                                       color="secondary"
                                       style={{paddingTop : '25px'}}
                                  >
                                 </Badge>
                                   </i>                                 
                              </a>
                              </li>
                      </ul>
                </div>  
                
            </nav>

           
        )
  }
}


const mapDispatchToProps = {
    logoutUser  : logoutUser
}

export default connect(null , mapDispatchToProps)(DashBoardHeader);