const express = require('express');
const passport = require('passport');
const router = express.Router();

const jwt = require('jsonwebtoken')
const keys = require('../../config/keys');
// AUTH ROUTES
        
        //  // GOOGLE OAUTH2 ROUTE

        //     router.get('/google' , passport.authenticate('google' , {
        //         scope : ['profile' , 'email'],
        //         session : false
        //     }));
        
        //      router.get(
        //          '/google/callback' ,  
        //           passport.authenticate('google' ) ,
            
        //           (req , res ) => {
            
        //                 const accessToken = token.generateAccessToken(req.user);
        //                 res.cookie('jwtAuth' , accessToken)

        //                 if(process.env.NODE_ENV === 'production') {
        //                      res.redirect('/dashboard');
        //                 }else {
        //                     res.redirect('/dashboard');
        //                 }
                       
        //           }              
        //         );



         // FACEBOOK OAUTH2 ROUTE
            router.get('/facebook' , passport.authenticate('facebook' , {
                session : false ,
                scope:[ 'email' ] 
               
            }));
            
            router.get(
                '/facebook/callback' ,

                 passport.authenticate('facebook' , {session : false}) ,
                 (req , res ) => {
                     console.log(req.user);
                     
                     const payload = { id : req.user.id , email : req.user.facebook.email   , username : req.user.facebook.username}
                
                     jwt.sign(
                         payload ,
                        keys.secretOrKey ,
                        {expiresIn : 3600000} ,
                        ( err , token ) => {
                            
                            // if 'jwt' cookie is already in client . just send this token in cookie and if not send new 
                             
                            res.cookie('jwt' , token , {httpOnly : false  , maxAge :365 * 24 * 60 * 60 * 1000 })
                            console.log(token);
                          
                            res.redirect('/dashboard')
                                
                          
                           }
             
                     )

                   
                  
                    
                            
              }  
                
                );


        // // LOCAL STRATEGY 

        //     router.post('/local/login', 
        //     passport.authenticate('local-login', { failureRedirect: '/auth/local/login' }),
        //     function(req, res) {
        //          res.redirect('/dashboard');
        //     });

        //     router.post('/local/signup', 
        //     passport.authenticate('local-signup', { failureRedirect: '/auth/local/signup' }),
        //     function(req, res) {
        //          res.redirect('/dashboard');
        //     });
        



         

            // user logout route
            


module.exports = router;
