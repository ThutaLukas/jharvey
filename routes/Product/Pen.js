
const express = require('express');
const router = express.Router();
const passport = require('passport');
const mongoose =require('mongoose');

const Pen = mongoose.model('pens');

// /pen/create  - PRIVATE ROUTE jwt authenticate
// adding pen product into database 

router.post('/create' ,

    passport.authenticate('jwt' , {session : false }) ,

   async (req , res ) => {
      

        try {

            const {code , name , description , categories , color , priceMM , priceUS , photos} = req.body;

            const newPen = new Pen({
                code : code , 
                name : name  ,
                description : description ,
                categories : categories,
                color : color ,
                priceMM : priceMM , 
                priceUS : priceUS ,
                photos : photos


            });

            await newPen.save();

            res.send(newPen)



            
        }catch(err) {

            res.send(err)

        }
       

    })


router.get('/get' ,

    (req , res ) => {

            Pen.find({} , (err , pens ) => {

               if(err) {
                   res.send(err)
               }

               res.send(pens)
            })



        
    })


router.get('/get/:id' , (req , res ) => {

            Pen.findOne({ _id : req.params.id} , (err , pen ) => {


                if(err) {
                    res.status(500).send(err)
                }
                else {
                    res.send(pen)
                }
            })



})




module.exports = router ;



