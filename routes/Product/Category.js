const express = require('express');
const router = express.Router();
const passport = require('passport');
const mongoose =require('mongoose')

const Category = mongoose.model('categories');



// adding category 
// private route  User ===  { admin : true }

router.post('/add' ,
 passport.authenticate('jwt' , {session : false}) ,
 async ( req , res ) => {


         try {
            const {name} = req.body;

            const category  =    new Category({
                name : name 
            });
        
           await category.save();       
            res.send(category)
        

         }catch(err){

            res.send(err);

         }
   
})



router.get('/get' , 

    passport.authenticate('jwt' , {session : false }) , 
     (req , res ) => {

            Category.find( {} , ( err , categories ) => {

                    if(err) {
                        res.json('something is wrong !')
                    }
                    res.json(categories)

            })


    })  







module.exports = 
 router ; 
