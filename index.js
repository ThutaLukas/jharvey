
const express = require('express');
const mongoose = require('mongoose');
const keys = require('./config/keys');
const passport = require('passport');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const path = require('path')

 // *** express server initialization ***
const app = express();


// MIDDLEWARES EXPRESS AND CUSTOM
    // body parsing middleware
    app.use(express.json());
     app.use(cookieParser())
     // cors issue solved for react client apps

    
     

    
    
      
      

   
   

    // passport.js middlware initialize and session initialize
    app.use(passport.initialize());
   
    



// DATABASE CONNECTION  AND HELPERS - mongodb , mongoose

        // add mongodb database uri in config folder
        mongoose.connect(keys.MONGO_URI);


// IMPORTING ( REQUIRE ) DATABASE MONGOOSE MODELS

    require('./models/User');
    require('./models/Category');
    require('./models/Pen');
    require('./models/Order');
    require('./models/Cart');
    require('./models/Store');
    



// ENABLING PASSPORT AUTH STRATEGIES ( FACEBOOK , GOOGLE) AND HELPERS IN ROOT INDEX.JS
    require('./services/passport');



// API ROUTE INITIATE FROM MAIN ROUTE
 require('./routes/index')(app);


 
    // Express will serve up production assets
    // like our main.js file, or main.css file!
    app.use(express.static('client/build'));
  
    // Express will serve up the index.html file
    // if it doesn't recognize the route
   
   app.get('*' , (req , res ) => {
        res.sendFile(path.resolve(__dirname , 'client' , 'build' , 'index.html'))
   });
  
  
  

// SERVER RUNNING PORT!
    const PORT = process.env.PORT || 5000;
    app.listen(PORT , ()=> {
        console.log(`node server is running up at port ${PORT}`);
    });
   