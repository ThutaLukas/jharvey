
const passport = require('passport');
const GoogleStrategy  = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const keys = require('../config/keys');
const mongoose = require('mongoose');

// User Model
const User = mongoose.model('users');
const Cart = mongoose.model('carts')


const cookieExtractor = function(req) {
    let token =  null;
    if (req && req.cookies) 
    {
        token = req.cookies['jwt'];
    }
    return token;
};


const opts = {};
opts.jwtFromRequest = cookieExtractor;
opts.secretOrKey = keys.secretOrKey;

//   // ENCODING USER
//     passport.serializeUser(( user , done ) => {
//         // we store user id into session 
//         done( null , user.id);
//     });
    
//  // DECODING USER this happen only after extract user id from cookie session   || this is where we get real user object (_id , googleId) finally ***
//      passport.deserializeUser((id , done) => {
//         User.findById(id)
//             .then((user) => {
//                     // passport send this user object as req.user
//                    done(null , user) ;
//             })
//      });


passport.use(

    new JWTStrategy(opts , ( jwt_payload , done ) => {
   
                   User.findById(jwt_payload.id)
                       .then((user) => {
                               if(user) {
   
                                   return done(null , user)
                               }
                               else {
                                   return done(null , false)
                               }
                       })
                       .catch(err => console.log(err));
   
   
    })
   
   )


//  //GOOGLE STRATEGY IMPLEMENTATION 
//     passport.use(
        
//             new GoogleStrategy({
//             clientID : keys.googleClientID ,
//             clientSecret : keys.googleClientScret,
//             callbackURL : `/auth/google/callback`,
//             proxy : true
            
//         },
//            async  (accessToken , refreshToken , profile ,   done ) => {
               
//                  const existingUser =   await User.findOne({'google.id': profile.id}) ; 
//                                 if(existingUser){
//                                         // we already has UserId , send it back
//                                         return done(null , existingUser );
//                                 }

//                                 const user = new User();
//                                  user.google.id = profile.id ;
//                                  user.google.username = profile.displayName ;
//                                  user.google.email = profile.emails[0].value;
//                                     const newUser = await user.save();
//                                 done(null , newUser );
                                      
                                   
                                               
//                     } )
                
//         );


// FACEBOOK STRATEGY IMPLEMENTAION 


passport.use(
        
    new FacebookStrategy({
    clientID : keys.FACEBOOK_APP_ID ,
    clientSecret : keys.FACEBOOK_APP_SECRET,

    callbackURL : `https://www.jharveypen.com/auth/facebook/callback`,
    profileFields   : [ 'emails' , 'displayName' , 'id'] ,  
    proxy : true,
    
    
    
},
   async  (accessToken , refreshToken , profile ,   done ) => {
       
         const existingUser =   await User.findOne({'facebook.id': profile.id}) ; 
                        if(existingUser){
                                // we already has UserId , send it back
                                return done(null , existingUser );
                        }
                     
                        const user =   await new User({
                            
                           'facebook.id' : profile.id ,
                           'facebook.username' : profile.displayName ,
                           'facebook.email' : profile.emails[0].value 
                          
                           

                       }).save();
                               
                        done(null , user );


                        if(user) {
                            const cart = new Cart();
                            cart.owner = user._id ;
                            await cart.save();

                        }
                                      
            } )
        
);








// //LOCAL STRATEGY IMPLEMENTATION

//         // LOCAL SIGN UP 
//             passport.use( 'local-signup' , new LocalStrategy(
//                 {
//                     usernameField: 'email',
//                     passwordField: 'password',
//                 } ,

//              async  function( req , email , password, done) {
                  
//                 const exisitingUser = await User.findOne({'local.email' : email });

//                 if(exisitingUser) {
//                     return done(null , false , {error : 'this email is already existed'} )
//                 }else {

//                     const user = new User();

//                      user.local.email = email ;
//                      user.local.password = user.generateHash(password);
//                      user.local.username = req.body.username;

//                      const newUser = await user.save();
//                      return done(null , newUser)
                
//                 }
              
//                 }
//             ));

//       // LOCAL SIGN IN  

//       passport.use( 'local-login' ,new LocalStrategy(
             
//             {
//                 usernameField: 'email',
//                 passwordField: 'password',

//             },

           
//         function(email, password, done) {
             
            
//             User.findOne({ 'local.email': email }, function (err, user) {
//                     if (err) { return done(err); }

//                     if (!user) { 
                        
//                         return done(null, false , {err : 'no user found'}); 
                    
//                     }
//                     if (!user.validPassword(password)) {
                    
//                          return done(null, false , {err : 'invalid password'}); 
                        
//                         }
//                     return done(null, user);
//                 });
//                 }
//       ));