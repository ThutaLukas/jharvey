

module.exports = {

    googleClientID : process.env.GOOGLE_CLIENT_ID,
    googleClientScret : process.env.GOOGLE_CLIENT_SECRET ,
    MONGO_URI : process.env.MONGO_URI ,
    cookieKey :  process.env.COOKIE_KEY ,
    stripePublishableKey : process.env.STRIPE_PUBLISHABLE_KEY ,
    stripeSecretKey : process.env.STRIPE_SECRET_KEY ,       
    sendGridKey : process.env.SENDGRID_KEY ,
    redirectDomain : process.env.REDIRECT_DOMAIN,
    FACEBOOK_APP_ID : process.env.FACEBOOK_APP_ID,
    FACEBOOK_APP_SECRET : process.env.FACEBOOK_APP_SECRET,
    secretOrKey : process.env.SECRET_OR_KEY
  

}